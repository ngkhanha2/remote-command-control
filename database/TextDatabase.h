/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TextDatabase.h
 * Author: Khanh
 *
 * Created on May 23, 2016, 8:19 PM
 */

#ifndef TEXTDATABASE_H
#define TEXTDATABASE_H

#include <fstream>

#include "Database.h"

using namespace std;

class TextDatabase: Database {
public:
	TextDatabase(const string &databasePath);
	virtual Person* Add(const string &idNumber, const string &firstName,
			const string &lastName, const string &location);
	virtual Person* Remove(const string &idNumber);
	virtual vector<Person>* List(const string &prefix);
	virtual bool isSaving();
	virtual ~TextDatabase();
private:
	bool saving;
	string databasePath;
	vector<Person*> persons;

	void load();
	void save();
};

#endif /* TEXTDATABASE_H */

