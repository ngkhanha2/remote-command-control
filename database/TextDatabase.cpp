/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TextDatabase.cpp
 * Author: Khanh
 * 
 * Created on May 23, 2016, 8:19 PM
 */

#include "TextDatabase.h"

using namespace std;

TextDatabase::TextDatabase(const string &databasePath) {
	this->databasePath = databasePath;
	this->saving = false;
	load();
}

void TextDatabase::load() {
	ifstream fi(this->databasePath.c_str());
	if (fi.good()) {
		string idNumber;
		string firstName;
		string lastName;
		string location;
		while (fi >> idNumber >> firstName >> lastName >> location) {
			this->persons.push_back(
					new Person(idNumber, firstName, lastName, location));
		}
	}
	fi.close();
}

bool TextDatabase::isSaving() {
	return this->saving;
}

Person* TextDatabase::Add(const string &idNumber, const string &firstName,
		const string &lastName, const string &location) {
	for (int i = 0; i < this->persons.size(); ++i) {
		if (this->persons[i]->GetIdNumber().compare(idNumber) == 0) {
			Person *p = this->persons[i];
			this->persons[i] = new Person(idNumber, firstName, lastName,
					location);
			delete p;
			this->save();
			return this->persons[i]->Clone();
		}
	}
	this->persons.push_back(
			new Person(idNumber, firstName, lastName, location));
	this->save();
	return this->persons[persons.size() - 1]->Clone();
}

Person* TextDatabase::Remove(const string &idNumber) {
	for (int i = 0; i < this->persons.size(); ++i) {
		if (this->persons[i]->GetIdNumber().compare(idNumber) == 0) {
			Person* p = this->persons[i];
			this->persons.erase(this->persons.begin() + i);
			this->save();
			return p;
		}
	}
	return NULL;
}

void TextDatabase::save() {
	this->saving = true;
	ofstream fo(this->databasePath.c_str());
	for (int i = 0; i < this->persons.size(); ++i) {
		fo << this->persons[i]->GetIdNumber() << " "
				<< this->persons[i]->GetFirstName() << " "
				<< this->persons[i]->GetLastName() << " "
				<< this->persons[i]->GetLocation() << endl;
	}
	fo.close();
	this->saving = false;
}

TextDatabase::~TextDatabase() {
	for (int i = 0; i < this->persons.size(); ++i) {
		delete this->persons[i];
	}
}

vector<Person>* TextDatabase::List(const string &prefix) {
	vector<Person> *result = new vector<Person>();
	for (int i = 0; i < this->persons.size(); ++i) {
		if (this->persons[i]->GetLastName().find(prefix) == 0) {
			result->push_back(
					Person(this->persons[i]->GetIdNumber(),
							this->persons[i]->GetFirstName(),
							this->persons[i]->GetLastName(),
							this->persons[i]->GetLocation()));
		}
	}
	return result;
}
