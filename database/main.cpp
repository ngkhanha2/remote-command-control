/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Khanh
 *
 * Created on May 23, 2016, 7:46 PM
 */

#include <cstdlib>
#include <iostream>

#include "TextDatabase.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
	TextDatabase dt = TextDatabase("database.txt");
	dt.Add("123", "Khanh", "Nguyen", "HCMC");
	dt.Add("1234", "Bao", "Nguyen", "HCMC");
	vector<Person>* l = dt.List("Ng");
	for (int i = 0; i < l->size(); ++i) {
		cout << (*l)[i].GetIdNumber();
	}
	return 0;
}

