/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Person.h
 * Author: Khanh
 *
 * Created on May 23, 2016, 8:20 PM
 */

#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person {
public:
	Person(string idNumber, string firstName, string lastName, string location);
	virtual ~Person();
	string GetLocation() const;
	string GetLastName() const;
	string GetFirstName() const;
	string GetIdNumber() const;
	Person* Clone();
private:
	string idNumber;
	string firstName;
	string lastName;
	string location;
};

#endif /* PERSON_H */

