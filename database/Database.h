/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Database.h
 * Author: Khanh
 *
 * Created on May 23, 2016, 8:18 PM
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <vector>

#include "Person.h"

using namespace std;

class Database {
public:
    virtual Person* Add(const string &idNumber, const string &firstName, const string &lastName, const string &location) = 0;
    virtual Person* Remove(const string &idNumber) = 0;
    virtual vector<Person>* List(const string &prefix) = 0;
    virtual bool isSaving() = 0;
private:

};

#endif /* DATABASE_H */

