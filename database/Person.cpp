/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Person.cpp
 * Author: Khanh
 * 
 * Created on May 23, 2016, 8:20 PM
 */

#include "Person.h"

Person::Person(string idNumber, string firstName, string lastName,
		string location) {
	this->idNumber = idNumber;
	this->firstName = firstName;
	this->lastName = lastName;
	this->location = location;
}

Person::~Person() {
}

string Person::GetLocation() const {
	return location;
}

string Person::GetLastName() const {
	return lastName;
}

string Person::GetFirstName() const {
	return firstName;
}

string Person::GetIdNumber() const {
	return idNumber;
}

Person* Person::Clone() {
	return new Person(this->GetIdNumber(), this->GetFirstName(),
			this->GetLastName(), this->GetLocation());
}
