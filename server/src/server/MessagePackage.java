/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public class MessagePackage {

    private Client client;
    private String message;

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    public MessagePackage(Client client, String message) {
        this.client = client;
        this.message = message;
    }
}
