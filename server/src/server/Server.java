/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public class Server {

    private int port;

    public int getPort() {
        return port;
    }

    protected Server(int port) {
        this.port = port;
    }

    public void start() {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(this.getPort());
            System.out.println("Listener is started on port " + this.getPort());
        } catch (IOException ex) {
            System.out.println("Cannot start listener on port " + this.getPort());
            System.exit(-1);
        }

        // Create and start ServerDispatcher thread
        ServerDispatcher serverDispatcher = new ServerDispatcher();
        serverDispatcher.start();

        while (true) {
            try {
                Socket socket = serverSocket.accept();
                Client client = Client.getClient(socket, serverDispatcher);
                if (client != null) {
                    client.connect();
                    serverDispatcher.addClient(client);
                }
            } catch (IOException ex) {
                
            }
        }

    }

    private static Server serverInstance;

    public static Server getServer() {
        return serverInstance;
    }

    public static Server getServer(int port) {
        if (serverInstance == null) {
            serverInstance = new Server(port);
        }
        return serverInstance;
    }
}
