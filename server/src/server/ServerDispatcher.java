/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.Console;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

import server.clienthandlers.Client;
import server.commandhandlers.CommandHandler;
import server.commandhandlers.CommandHandlerList;
import server.database.Database;

/**
 *
 * @author Hien Nguyen
 */
public class ServerDispatcher extends Thread {

    private Vector<Client> clients = new Vector<Client>();
    private Vector<MessagePackage> commandQueue = new Vector<MessagePackage>();

    public synchronized void addClient(Client client) {
        this.clients.add(client);
    }

    public synchronized void deleteClient(Client client) {
        int index = clients.indexOf(client);
        if (index >= 0) {
            clients.removeElementAt(index);
        }
    }

    public synchronized void dispatchMessage(Client client, String message) {
        commandQueue.add(new MessagePackage(client, message));
        notify();
    }

    private synchronized MessagePackage getNextCommandFromQueue() throws InterruptedException {
        while (commandQueue.isEmpty()) {
            wait();
        }
        MessagePackage messagePackage = commandQueue.get(0);
        commandQueue.removeElementAt(0);
        return messagePackage;
    }

    private synchronized void processMessagePackage(MessagePackage messagePackage) throws InterruptedException {
        int spaceIndex = messagePackage.getMessage().trim().indexOf(" ");
        String commandName = null;
        if (spaceIndex >= 0) {
            commandName = messagePackage.getMessage().trim().substring(0, spaceIndex);
        } else {
            commandName = messagePackage.getMessage().trim();
        }
        System.out.println("Message from "
                + messagePackage.getClient().getSocket().getInetAddress().getHostName() + ":"
                + messagePackage.getClient().getSocket().getPort() + " at " + (new Date())
                + ": " + messagePackage.getMessage());
        CommandHandler commandHandler = (new CommandHandlerList()).getCommandObject(
                commandName,
                messagePackage.getClient(),
                messagePackage.getMessage());
        if (commandHandler != null) {
            while (Database.getDatabase().isSaving()) {
                wait();
            }
            commandHandler.process();
        }
    }

    public void run() {
        try {
            while (true) {
                MessagePackage messagePackage = getNextCommandFromQueue();
                processMessagePackage(messagePackage);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerDispatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
