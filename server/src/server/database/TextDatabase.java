/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bao Nguyen
 */
public class TextDatabase extends Database {

    private boolean _isSaving = false;
    private Map<String, Person> personsMap = new HashMap<String, Person>();
    private String databasePath;

    public TextDatabase(String databasePath) {
        this.databasePath = databasePath;
        this.load();
    }

    private void load() {
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            fis = new FileInputStream(this.databasePath);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            while (br.ready()) {
                String line = br.readLine();
                String personInfo[] = line.split(" ");
                personsMap.put(personInfo[0], new Person(personInfo[0], personInfo[1], personInfo[2], personInfo[3]));
            }
            br.close();
            isr.close();
            fis.close();
        } catch (Exception ex) {
            Logger.getLogger(TextDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Person add(String idNumber, String firstName, String lastName, String location) {
        Person person = new Person(idNumber, firstName, lastName, location);
        if (personsMap.containsKey(idNumber)) {
            personsMap.replace(idNumber, person);
        } else {
            personsMap.put(idNumber, person);
        }
        this.save();
        return person;
    }

    @Override
    public Person remove(String idNumber) {
        if (personsMap.containsKey(idNumber)) {
            Person person = personsMap.get(idNumber);
            personsMap.remove(idNumber);
            this.save();
            return person;
        }
        return null;
    }

    @Override
    public Vector<Person> list(String prefix) {
        Vector<Person> persons = new Vector<Person>();
        for (Map.Entry<String, Person> entry : personsMap.entrySet()) {
            if (entry.getValue().getLastName().indexOf(prefix) == 0) {
                persons.add(entry.getValue());
            }
        }
        return persons;
    }

    @Override
    protected void save() {
        this._isSaving = true;
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(this.databasePath, false);
            osw = new OutputStreamWriter(fos);
            bw = new BufferedWriter(osw);
            for (Map.Entry<String, Person> entry : personsMap.entrySet()) {
                bw.write(entry.getValue().getIdNumber() + " " + entry.getValue().getFirstName() + " " + entry.getValue().getLastName() + " " + entry.getValue().getLocation());
                bw.write("\n");
            }
            bw.close();
            osw.close();
            fos.close();
        } catch (Exception ex) {
            Logger.getLogger(TextDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        this._isSaving = false;
    }

    @Override
    public boolean isSaving() {
        return this._isSaving;
    }

}
