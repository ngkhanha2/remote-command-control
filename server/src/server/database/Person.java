/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.database;

/**
 *
 * @author Bao Nguyen
 */
public class Person {

    private String firstName;
    private String lastName;
    private String idNumber;
    private String location;

    public Person(String idNumber, String firstName, String lastName, String location) {
        this.idNumber = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.location = location;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return the idNumber
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    public String getFullName() {
        return this.getFirstName() + " " + this.getLastName();
    }

}
