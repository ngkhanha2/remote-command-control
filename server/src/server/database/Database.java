/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.database;

import java.util.Vector;

/**
 *
 * @author Bao Nguyen
 */
public abstract class Database {

    public abstract Person add(String idNumber, String firstName, String lastBame, String location);

    public abstract Person remove(String idNumber);

    public abstract Vector<Person> list(String prefix);

    protected abstract void save();

    public abstract boolean isSaving();

    private static Database databaseInstance;

    public static Database getDatabase() {
        if (databaseInstance == null) {
            databaseInstance = new TextDatabase("database.txt");
        }
        return databaseInstance;
    }
}
