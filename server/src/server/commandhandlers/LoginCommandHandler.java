/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public class LoginCommandHandler extends CommandHandler {

    public LoginCommandHandler() {
        super();
    }

    public LoginCommandHandler(Client client, String command) {
        super(client, command);
    }

    @Override
    public void process() {
        this.getClient().getClientSender().send("Hello");
    }

    @Override
    public String commandName() {
        return "login";
    }

    @Override
    public CommandHandler Clone(Client client, String command) {
        return new LoginCommandHandler(client, command);
    }

}
