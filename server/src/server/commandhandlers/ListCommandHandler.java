/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import java.util.Vector;
import server.clienthandlers.Client;
import server.database.Database;
import server.database.Person;

/**
 *
 * @author Hien Nguyen
 */
public class ListCommandHandler extends CommandHandler {

    public ListCommandHandler() {
        super();
    }

    public ListCommandHandler(Client client, String command) {
        super(client, command);
    }

    private String getCell(String value) {
        String space = "";
        int n = 20 - value.length();
        while (n > 0) {
            space += " ";
            --n;
        }
        return value + space;
    }

    @Override
    public void process() {
        String[] args = this.getCommand().split(" ");
        if (args.length != 2) {
            this.getClient().getClientSender().send("Command is invalid.");
        }
        Vector<Person> persons = Database.getDatabase().list(args[1]);
        String resultString = "\n" + this.getCell("ID NUMBER") + "|" + this.getCell("FIRST NAME") + "|" + this.getCell("LAST NAME") + "|" + this.getCell("LOCATION") + "\n"
                + "-----------------------------------------------------------------------------------\n";
        for (Person p : persons) {
            resultString += this.getCell(p.getIdNumber()) + "|" + this.getCell(p.getFirstName()) + "|" + this.getCell(p.getLastName()) + "|" + this.getCell(p.getLocation()) + "\n";
        }
        this.getClient().getClientSender().send(resultString);
    }

    @Override
    public String commandName() {
        return "list";
    }

    @Override
    public CommandHandler Clone(Client client, String command) {
        return new ListCommandHandler(client, command);
    }

}
