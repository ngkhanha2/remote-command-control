/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import server.clienthandlers.Client;
import server.database.Database;
import server.database.Person;

/**
 *
 * @author Hien Nguyen
 */
public class AddCommandHandler extends CommandHandler {

    public AddCommandHandler() {
        super();
    }

    public AddCommandHandler(Client client, String command) {
        super(client, command);
    }

    @Override
    public void process() {
        String[] args = this.getCommand().split(" ");
        if (args.length != 5) {
            this.getClient().getClientSender().send("Command is invalid.");
        }
        Person p = Database.getDatabase().add(args[3], args[1], args[2], args[4]);
        if (p != null) {
            this.getClient().getClientSender().send("Add person " + p.getIdNumber() + " - " + p.getFullName() + " successfully.");
        } else {
            this.getClient().getClientSender().send("Failed to add person " + args[3] + ".");
        }
    }

    @Override
    public String commandName() {
        return "add";
    }

    @Override
    public CommandHandler Clone(Client client, String command) {
        return new AddCommandHandler(client, command);
    }

}
