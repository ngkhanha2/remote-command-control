/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import server.clienthandlers.Client;
import server.database.Database;
import server.database.Person;

/**
 *
 * @author Hien Nguyen
 */
public class RemoveCommandHandler extends CommandHandler {

    public RemoveCommandHandler() {
        super();
    }

    public RemoveCommandHandler(Client client, String command) {
        super(client, command);
    }

    @Override
    public void process() {
        String[] args = this.getCommand().split(" ");
        if (args.length != 2) {
            this.getClient().getClientSender().send("Command is invalid.");
        }
        Person p = Database.getDatabase().remove(args[1]);
        if (p != null) {
            this.getClient().getClientSender().send("Remove person " + p.getIdNumber() + " - " + p.getFullName() + " successfully.");
        } else {
            this.getClient().getClientSender().send("Failed to remove person " + args[1] + ".");
        }
    }

    @Override
    public String commandName() {
        return "remove";
    }

    @Override
    public CommandHandler Clone(Client client, String command) {
        return new RemoveCommandHandler(client, command);
    }
}
