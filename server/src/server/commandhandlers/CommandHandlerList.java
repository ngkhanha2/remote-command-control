/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import java.util.Vector;
import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public class CommandHandlerList {

    private Vector<CommandHandler> commands = new Vector<CommandHandler>();

    public CommandHandlerList() {
        commands.add(new LoginCommandHandler());
        commands.add(new QuitCommandHandler());
        commands.add(new AddCommandHandler());
        commands.add(new RemoveCommandHandler());
        commands.add(new ListCommandHandler());
    }

    public CommandHandler getCommandObject(String commandName, Client client, String fullCommand) {
        for (int i = 0; i < this.commands.size(); ++i) {
            if (this.commands.get(i).commandName().equalsIgnoreCase(commandName)) {
                return this.commands.get(i).Clone(client, fullCommand);
            }
        }
        return null;
    }
}
