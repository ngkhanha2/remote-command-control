/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public class QuitCommandHandler extends CommandHandler {

    public QuitCommandHandler() {
        super();
    }

    public QuitCommandHandler(Client client, String command) {
        super(client, command);
    }

    @Override
    public void process() {
        this.getClient().getClientSender().send("goodbye");
        this.getClient().disconnect();
    }

    @Override
    public String commandName() {
        return "quit";
    }

    @Override
    public CommandHandler Clone(Client client, String command) {
        return new QuitCommandHandler(client, command);
    }

}
