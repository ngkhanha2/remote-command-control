/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.commandhandlers;

import server.clienthandlers.Client;

/**
 *
 * @author Hien Nguyen
 */
public abstract class CommandHandler {

    private Client client;
    private String command;

    public CommandHandler() {

    }

    public CommandHandler(Client client, String command) {
        this.client = client;
        this.command = command;
    }

    public abstract void process();

    public abstract String commandName();

    public abstract CommandHandler Clone(Client client, String command);

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }
}
