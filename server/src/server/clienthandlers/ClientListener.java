/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.clienthandlers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerDispatcher;

/**
 *
 * @author Hien Nguyen
 */
public class ClientListener extends ClientHandler {

    private BufferedReader bufferedReader;

    public ClientListener(Client client) throws IOException {
        super(client);
        this.bufferedReader = new BufferedReader(new InputStreamReader(this.getClient().getSocket().getInputStream()));
    }

    public void run() {
        try {
            while (!isInterrupted()) {
                String message = bufferedReader.readLine();
                if (message == null) {
                    break;
                }
                this.getClient().getServerDispatcher().dispatchMessage(this.getClient(), message);
            }
        } catch (IOException ex) {
//            Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getClient().disconnect();
    }
}
