/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.clienthandlers;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerDispatcher;

/**
 *
 * @author Hien Nguyen
 */
public class Client {

    private Socket socket;
    private ClientListener clientListener = null;
    private ClientSender clientSender = null;
    private ServerDispatcher serverDispatcher;

    protected Client(Socket socket, ServerDispatcher serverDispatcher) {
        this.socket = socket;
        this.serverDispatcher = serverDispatcher;
    }

    public static Client getClient(Socket socket, ServerDispatcher serverDispatcher) {
        try {
            Client client = new Client(socket, serverDispatcher);
            client.clientListener = new ClientListener(client);
            client.clientSender = new ClientSender(client);
            return client;
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * @return the clientListener
     */
    public ClientListener getClientListener() {
        return clientListener;
    }

    /**
     * @return the clientSender
     */
    public ClientSender getClientSender() {
        return clientSender;
    }

    public void connect() {
        // In each client object, start listener and sender threads
        this.getClientListener().start();
        this.getClientSender().start();
    }

    /**
     * @return the serverDispatcher
     */
    public ServerDispatcher getServerDispatcher() {
        return serverDispatcher;
    }

    public void disconnect() {
        this.getClientListener().interrupt();
        this.getClientSender().interrupt();
        this.getServerDispatcher().deleteClient(this);
        try {
            this.getSocket().close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
