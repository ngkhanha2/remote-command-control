/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.clienthandlers;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerDispatcher;

/**
 *
 * @author Hien Nguyen
 */
public class ClientSender extends ClientHandler {

    private Vector<String> messagesQueue = new Vector<String>();
    private PrintWriter printWriter;

    public ClientSender(Client client) throws IOException {
        super(client);
        this.printWriter = new PrintWriter(new OutputStreamWriter(this.getClient().getSocket().getOutputStream()));
    }

    public synchronized void send(String message) {
        messagesQueue.add(message);
        notify();
    }

    private synchronized String getNextMessageFromQueue() throws InterruptedException {
        while (messagesQueue.isEmpty()) {
            wait();
        }
        String message = messagesQueue.get(0);
        messagesQueue.removeElementAt(0);
        return message;
    }

    private void sendMessageToClient(String message) {
        printWriter.println(message);
        printWriter.flush();
    }

    public void run() {
        try {
            while (!isInterrupted()) {
                String message = getNextMessageFromQueue();
                sendMessageToClient(message);
            }
        } catch (InterruptedException ex) {
//            Logger.getLogger(ClientSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getClient().disconnect();
    }

}
