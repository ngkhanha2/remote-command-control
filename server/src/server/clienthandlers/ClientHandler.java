/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.clienthandlers;

import server.ServerDispatcher;

/**
 *
 * @author Hien Nguyen
 */
public abstract class ClientHandler extends Thread {

    private Client client;

    public ClientHandler(Client client) {
        this.client = client;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }
}
