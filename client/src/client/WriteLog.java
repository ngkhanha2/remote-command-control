/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Bao Nguyen
 */
public class WriteLog {

    private static WriteLog writeLogInstance;
    private String filePath;

    protected WriteLog(String filePath) {
        this.filePath = filePath;
    }

    public void Write(String string) {
        Write(string, true);
    }

    public void Write(String string, boolean showInConsole) {
        if (showInConsole) {
            System.out.println(string);
        }
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(this.filePath, true);
            stream.write((string + "\n").getBytes());
            stream.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                stream.close();
            } catch (IOException ex) {

            }
        }
    }

    public static WriteLog getWriteLog() {
        if (writeLogInstance == null) {
            writeLogInstance = new WriteLog("client.log");
        }
        return writeLogInstance;
    }
}
