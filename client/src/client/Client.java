/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author Bao Nguyen
 */
public class Client {

    private String hostName;
    private int port;

    protected Client(String hostName, int port) {
        this.hostName = hostName;
        this.port = port;
    }

    public void start() {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            Socket socket = new Socket(this.getHostName(), this.getPort());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            WriteLog.getWriteLog().Write("Connected to server " + this.getHostName() + ":" + this.getPort());
        } catch (IOException ex) {
            WriteLog.getWriteLog().Write("Cannot connect to server " + this.getHostName() + ":" + this.getPort());
            System.out.println();
            System.exit(0);
        }

        // Create and start Sender thread
        Sender sender = new Sender(out);
        sender.setDaemon(true);
        sender.start();

        try {
            // Read messages from the server and print them
            String message;
            while ((message = in.readLine()) != null) {
                WriteLog.getWriteLog().Write("Message from server at " + (new Date()) + ": " + message);
                if (message.equalsIgnoreCase("goodbye")) {
                    sender.interrupt();
                    System.exit(0);
                }
            }
        } catch (IOException ex) {
            System.err.println("Connection to server broken.");
        }
    }

    /**
     * @return the hostName
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    private static Client clientInstance;

    public static Client geClient(String hostName, int port) {
        if (clientInstance == null) {
            clientInstance = new Client(hostName, port);
        }
        return clientInstance;
    }
}
