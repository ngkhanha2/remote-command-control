/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 *
 * @author Bao Nguyen
 */
public class Sender extends Thread {

    private PrintWriter printWriter;

    public Sender(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            while (!isInterrupted()) {
                String message = in.readLine();
                WriteLog.getWriteLog().Write("> " + message, false);
                printWriter.println(message);
                printWriter.flush();
            }
        } catch (IOException ex) {

        }
    }
}
